//
//  ConstantsUsedInProject.swift
//  MovieDetails
//
//  Created by Naveen Natrajan on 20/07/22.
//

import Foundation

struct ConstantsUsedInProject {
    static let baseUrl = "https://api.github.com/repositories"
}



struct segueIdentifiers {
    static let mainViewToDetailView = "mainToDetailView"
}
